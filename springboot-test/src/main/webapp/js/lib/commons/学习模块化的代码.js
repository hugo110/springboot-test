/**
 * js 的话 应该就不适合和java 一样这么多类了，要用一个统一的对象来处理这个事情
 * 
 * 那我就单独创建一个对象，然后  引用  后面再起一个好的名字
 * 
 * 全局变量叫做tilde.js
 * tilde  使用这个为调用方法  
 */
(function() {
	var root = this;
	// Save the previous value of the `_` variable.
	var previousTilde = root.tilde;
	var tilde = function(obj) {
		if(obj instanceof tilde) return obj;
		if(!(this instanceof tilde)) return new tilde(obj);
		this._wrapped = obj;
	};
	if(typeof exports !== 'undefined') {  
		if(typeof module !== 'undefined' && module.exports) {
			exports = module.exports = tilde;
		}
		exports.tilde = tilde;
	} else {
		root.tilde = tilde;
	}


	tilde.version = "1.0.0";
	if(typeof define === 'function' && define.amd) {
		define('tilde', [], function() {
			return tilde;
		});
	}
}.call(this));
/*var  DataUtil = {};

(function() {
	Date.prototype.format = function(fmt, dateTime) { //author: meizz 
		var date = dateTime;
		if(!fmt) {
			fmt = "yyyy-MM-dd hh:mm:ss";
		}
		if(!date) {

			date = new Date();
		}
		if(typeof(dateTime) == 'number') { //如果是数字  那么 进行转换 
			date = new Date(date);
		}
		//下面是开始替换 
		var o = {
			"M+": date.getMonth() + 1, //月份   
			"d+": date.getDate(), //日   
			"h+": date.getHours(), //小时   
			"m+": date.getMinutes(), //分   
			"s+": date.getSeconds(), //秒   
			"q+": Math.floor((date.getMonth() + 3) / 3), //季度   
			"S": date.getMilliseconds() //毫秒   
		};
		if(/(y+)/.test(fmt))
			fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
		for(var k in o)
			if(new RegExp("(" + k + ")").test(fmt))
				fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
		return fmt;
	}
})();*/