/**
 * 基于_.js 进行扩展
 * 基本上通用的代码，尽量扩展到这里面
 * 
 */
(function() {
	/*格式化日期     如果什么都不传，默认配置当前
	 * dateTime 日期或者日期数值
	 * fmt  格式化的选择   默认  "yyyy-MM-dd hh:mm:ss"
	 */
	_.mixin({
		formatDate: function(dateTime, fmt) { //author: meizz 
			var date = dateTime;
			if(!fmt) {
				fmt = "yyyy-MM-dd hh:mm:ss";
			}
			if(!date) {
				date = new Date();
			}
			if(typeof(dateTime) == 'number') { //如果是数字  那么 进行转换 
				date = new Date(date);
			}
			//下面是开始替换 
			var o = {
				"M+": date.getMonth() + 1, //月份   
				"d+": date.getDate(), //日   
				"h+": date.getHours(), //小时   
				"m+": date.getMinutes(), //分   
				"s+": date.getSeconds(), //秒   
				"q+": Math.floor((date.getMonth() + 3) / 3), //季度   
				"S": date.getMilliseconds() //毫秒   
			};
			if(/(y+)/.test(fmt))
				fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
			for(var k in o)
				if(new RegExp("(" + k + ")").test(fmt))
					fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
			return fmt;
		}
	});
	/*   把数字金额转换为(中文大写)金额.
	 */
	_.mixin({
		convertCurrency: function(currencyDigits) {
			var MAXIMUM_NUMBER = 99999999999.99;
			var CN_ZERO = "零";
			var CN_ONE = "壹";
			var CN_TWO = "贰";
			var CN_THREE = "叁";
			var CN_FOUR = "肆";
			var CN_FIVE = "伍";
			var CN_SIX = "陆";
			var CN_SEVEN = "柒";
			var CN_EIGHT = "捌";
			var CN_NINE = "玖";
			var CN_TEN = "拾";
			var CN_HUNDRED = "佰";
			var CN_THOUSAND = "仟";
			var CN_TEN_THOUSAND = "万";
			var CN_HUNDRED_MILLION = "亿";
			var CN_DOLLAR = "元";
			var CN_TEN_CENT = "角";
			var CN_CENT = "分";
			var CN_INTEGER = "整";

			var integral;
			var decimal;
			var outputCharacters;
			var parts;
			var digits, radices, bigRadices, decimals;
			var zeroCount;
			var i, p, d;
			var quotient, modulus;
			currencyDigits += ""; //兼容纯数字
			// 验证是否为数字
			if(currencyDigits.match(/[^,.\d]/) != null) {
				alert("输入内容不能包含字符串!");
				return "";
			}

			// 验证输入内容
			if((currencyDigits).match(/^(([1-9]{1}\d*)|([0]{1}))(\.(\d){1,2})?$/) == null) {
				alert("输入内容不合法!");
				return "";
			}

			currencyDigits = currencyDigits.replace(/,/g, "");
			currencyDigits = currencyDigits.replace(/^0+/, "");

			if(Number(currencyDigits) > MAXIMUM_NUMBER) {
				alert("输入的数字过大!");
				return "";
			}

			parts = currencyDigits.split(".");
			if(parts.length > 1) {
				integral = parts[0];
				decimal = parts[1];

				decimal = decimal.substr(0, 2);
			} else {
				integral = parts[0];
				decimal = "";
			}

			digits = new Array(CN_ZERO, CN_ONE, CN_TWO, CN_THREE, CN_FOUR, CN_FIVE, CN_SIX, CN_SEVEN, CN_EIGHT, CN_NINE);
			radices = new Array("", CN_TEN, CN_HUNDRED, CN_THOUSAND);
			bigRadices = new Array("", CN_TEN_THOUSAND, CN_HUNDRED_MILLION);
			decimals = new Array(CN_TEN_CENT, CN_CENT);

			outputCharacters = "";

			if(Number(integral) > 0) {
				zeroCount = 0;
				for(i = 0; i < integral.length; i++) {
					p = integral.length - i - 1;
					d = integral.substr(i, 1);
					quotient = p / 4;
					modulus = p % 4;
					if(d == "0") {
						zeroCount++;
					} else {
						if(zeroCount > 0) {
							outputCharacters += digits[0];
						}
						zeroCount = 0;
						outputCharacters += digits[Number(d)] + radices[modulus];
					}
					if(modulus == 0 && zeroCount < 4) {
						outputCharacters += bigRadices[quotient];
					}
				}
				outputCharacters += CN_DOLLAR;
			}

			if(decimal != "") {
				for(i = 0; i < decimal.length; i++) {
					d = decimal.substr(i, 1);
					if(d != "0") {
						outputCharacters += digits[Number(d)] + decimals[i];
					}
				}
			}

			if(outputCharacters == "") {
				outputCharacters = CN_ZERO + CN_DOLLAR;
			}
			if(decimal == "") {
				outputCharacters += CN_INTEGER;
			}
			outputCharacters = outputCharacters;
			return outputCharacters;
		}
	});
}.call(this));