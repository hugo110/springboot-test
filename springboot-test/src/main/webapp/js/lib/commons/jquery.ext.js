/**
 * 对juqey的一些扩展
 */
// IIFE(立即调用函数表达式);  [参考 http://suqing.iteye.com/blog/1981591/]
;
(function($) {
	// 扩展这个方法到jQuery.
	// $.extend() 是吧方法扩展到 $ 对象上，和 $.fn.extend 不同。 扩展到 $.fn.xxx 上后，
	// 调用的时候就可以是 $(selector).xxx()
	$.fn.extend({
		// 表单 name 反序列化
		deserialize: function(data) {
			Object.keys(data).map(function(currentValue, index, arr) {
				this.find("input[name=" + currentValue + "]").val(data[currentValue]);
			});
			// 遍历匹配元素的集合
			// 注意这里有个"return"，作用是把处理后的对象返回，实现链式操作
			//          return this.each(function () {
			//              // 在这里编写相应的代码进行处理
			//          });
			return this;
		}
	});
	// 传递jQuery到内层作用域去, 如果window,document用的多的话, 也可以在这里传进去.
	// })(jQuery, window, document, undefined);
})(jQuery, undefined);
// 调用方式 $(".selector").pluginName().otherMethod();
