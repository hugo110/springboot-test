/**
 * 各种原生类 增加原型方法的 维护类
 */
// ============================ Object ===============================
(function() {
	/**
	 * "".isEmpty() // true
	 * "".isEmpty("")// true
	 */
//	Object.prototype.isEmpty = function() {
//		return this.length == 0;
//	}
//	/**
//	 * 如果这个值 为空则返回一个默认值 "".defaultIfNull(22) // 22
//	 */
//	Object.prototype.defaultIfNull = function(value) {
//		return this.length == 0 ? value : this;
//	}
})();
// ============================ String ===============================
(function() {
	/**
	 * 忽略大小写比较字符串是否相等
	 */
	String.prototype.equalsIgnoreCase = function(anotherString) {
		if(this === anotherString) { // 如果两者相同 否则判端两个的大小写是否为null
			return true;
		}
		// 因为 typeof(null) = object typeof(undefined) = undefined 实际上也是判端了这两个不为空
		if(typeof(anotherString) === 'string') {
			return this.toLowerCase() == anotherString.toLowerCase(); //
		}
		return false;
	}
		/**
	 * " ".isBlank() // true
	 */
//	String.prototype.isBlank = function() {
//		return this.trim().length == 0;
//	}
})();
// ============================ Date ===============================
