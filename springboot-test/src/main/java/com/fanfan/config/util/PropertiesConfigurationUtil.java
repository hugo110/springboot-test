package com.fanfan.config.util;

import org.apache.commons.configuration.AbstractConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.commons.io.Charsets;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PropertiesConfigurationUtil {
    /**
     * //注意路径默认指向的是classpath的根目录
     * 
     * @Title getPropertiesConfiguration
     * @author 于国帅
     * @date 2018年3月2日 下午4:37:37
     * @param fileName
     * @return PropertiesConfiguration
     */
    public static PropertiesConfiguration getPropertiesConfiguration(String fileName) {
        AbstractConfiguration.setDefaultListDelimiter(';'); // 默认列表分隔符 会影响到后续创建的对象使用分隔符
        PropertiesConfiguration config = null;
        try {
            config = new PropertiesConfiguration(fileName);
            config.setReloadingStrategy(new FileChangedReloadingStrategy()); // 文件发生改变时重新加载
            config.setAutoSave(true); // 修改属性之后自动保存。
            config.setEncoding(Charsets.UTF_8.toString()); // 默认是ISO-8859-1
        } catch (ConfigurationException e) {
            log.error("getPropertiesConfiguration ", e);
        }
        return config;
    }

    /* //注释的原因的时 暂时用不到，想不到更好的设计的方式  
     *  public static PropertiesConfiguration getPropertiesConfiguration(URL url) {
        PropertiesConfiguration config = null;
        try {
            config = new PropertiesConfiguration(url);
        } catch (ConfigurationException e) {
            log.error("getPropertiesConfiguration", e);
        }
        return config;
    }
    
    public static PropertiesConfiguration getPropertiesConfiguration(File file) {
        PropertiesConfiguration config = null;
        try {
            config = new PropertiesConfiguration(file);
        } catch (ConfigurationException e) {
            log.error("getPropertiesConfiguration", e);
        }
        return config;
    }*/
}
