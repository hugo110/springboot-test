package com.fanfan.listener;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * 依赖于spring框架，在spring启动时调用 接近于加载完所有spring相关的启动
 * 
 * @ClassName ApplicationStartup
 * @author <a href="892042158@qq.com" target="_blank">于国帅</a>
 * @date 2018年5月4日 下午11:10:31
 *
 */
@Slf4j
@Service // 两种初始化方法都没法直接使用@Autowired来注入sevice进行数据库操作，于是我只好另辟蹊径，从spring的beanfactory中取得serice进行DB访问
public class ApplicationStartup implements ApplicationListener<ContextRefreshedEvent> {

//    private SubmitAccessTimeTask accessSubtask;
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (event.getApplicationContext().getParent() != null) {
            return;
        }
        log.error("ApplicationStartup 启动了。。。。。。。。。。。");
//        ApplicationContext applicationContext = event.getApplicationContext();
//        WebApplicationContext wac = (WebApplicationContext) applicationContext;
//        ServletContext context = wac.getServletContext();
//        RequestMappingHandlerMapping bean = wac.getBean(RequestMappingHandlerMapping.class);// 通过上下文对象获取RequestMappingHandlerMapping实例对象
//        Map<RequestMappingInfo, HandlerMethod> handlerMethods = bean.getHandlerMethods();
//        context.setAttribute("requestMappingUrls", handlerMethods);
        /**
         * AcctimeBatchModel acctimeBatchModel = (AcctimeBatchModel) context.getAttribute(WebConfig.ACCESS_TIME_BATCH);
         * System.out.println("============="+acctimeBatchModel); accessSubtask = new SubmitAccessTimeTask(1000 *30 * 1L);
         * accessSubtask.execute(acctimeBatchModel);
         **/
    }
}