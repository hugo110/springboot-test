package com.fanfan.example.springmvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fanfan.example.redis.IRedis;

@RestController
@RequestMapping("beanTest")
public class BeanTest {
//    @Autowired
//    @Qualifier("serviceImpl1")
//    ISerivce service1;
//    @Resource(name = "serviceImpl2")
//    ISerivce service2;
    @Autowired
//    @Qualifier("redisImp2")
    IRedis service1;

//    =======测试输出多个service 时 注入的service是哪个
    @RequestMapping("")
    public void test() {
        System.err.println(service1);
//        System.err.println(service2);

    }

}
