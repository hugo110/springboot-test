package com.fanfan.example.springmvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fanfan.example.model.JsonResult;

/**
 * 我想要做的是 前台传递多个相同的对象转变到我的controller
 * 
 * @第二 我想要知道根据页面如何刷新下表
 * 
 * @ClassName MoreObjectAction
 * @author <a href="892042158@qq.com" target="_blank">于国帅</a>
 * @date 2019年1月16日 下午4:22:36
 *
 */
@Controller
@RequestMapping("moreObject")
public class MoreObjectAction {
    @RequestMapping("toTest")
    public String toTest() {
        // 转变对应的数据

        return "";
    }

    @RequestMapping("test")
    @ResponseBody
    public JsonResult test() {
        // 转变对应的数据

        return JsonResult.sendOk("");
    }
}
