package com.fanfan.example.model;

import org.springframework.http.HttpStatus;

import lombok.Data;

/**
 * json 返回工具类 配合layui
 * 
 * @ClassName JsonResult
 * @author <a href="892042158@qq.com" target="_blank">于国帅</a>
 * @date 2018年11月15日 上午1:09:56
 *
 */
@Data
public class JsonResult {
    private int code; // 状态位
    private long count; // 数据有多少条
    private String msg; // 提示消息
    private Object data; // 返回的数据

    public JsonResult() {

    }

    public JsonResult(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public JsonResult(int code, long count, String msg, Object data) {
        this.code = code;
        this.count = count;
        this.msg = msg;
        this.data = data;
    }

    /**
     * 
     * 
     * @Title sendOk
     * @author 于国帅
     * @date 2018年11月18日 下午5:21:23
     * @param msg
     * @param obj
     * @return JsonResult
     */
    public static JsonResult sendOk(String msg, Object obj) {
        return new JsonResult(HttpStatus.OK.value(), msg, obj);
    }

    public static JsonResult sendOk(Object obj) {
        return new JsonResult(HttpStatus.OK.value(), "ok", obj);
    }

    public static JsonResult sendOk(String msg) {
        return new JsonResult(HttpStatus.OK.value(), msg, null);
    }

    /**
     * 
     * 
     * @Title sendError
     * @author 于国帅
     * @date 2018年11月18日 下午5:22:26
     * @param msg
     * @param obj
     * @return JsonResult
     */
    public static JsonResult sendError(String msg, Object obj) {
        return new JsonResult(HttpStatus.INTERNAL_SERVER_ERROR.value(), msg, obj);
    }

    public static JsonResult sendError(Object obj) {
        return new JsonResult(HttpStatus.INTERNAL_SERVER_ERROR.value(), "error", obj);
    }

    public static JsonResult sendError(String msg) {
        return new JsonResult(HttpStatus.INTERNAL_SERVER_ERROR.value(), msg, null);
    }

}
