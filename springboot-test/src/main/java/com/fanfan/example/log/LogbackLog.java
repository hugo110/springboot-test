//package com.fanfan.example.log;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//
//@Controller
//public class LogbackLog {
//    private final static Logger log = LoggerFactory.getLogger(LogbackLog.class);
//
//    /**
//     * @see <a>https://www.cnblogs.com/lspz/p/6473686.html</a> 配置详解 <a>http://blog.csdn.net/haidage/article/details/6794509</a>
//     *      TODO(这里用一句话描述这个方法的作用)
//     * @Title home
//     * @author 于国帅
//     * @date 2017年11月24日 上午11:34:43
//     * @return String
//     */
//    @RequestMapping("/")
//    public String home() {
//        log.trace("======trace");
//        log.debug("======debug");
//        log.info("======info");
//        log.warn("======warn");
//        log.error("======error");
//        return "jsp/index";
//    }
//
//    /**
//     */
//    @RequestMapping("/testAnnotationLogbackLog")
//    public String AnnotationLogbackLog() {
//        AnnotationLogbackLog annotationLogbackLog = new AnnotationLogbackLog();
//        annotationLogbackLog.testAnnotationLogbackLog();
//        return "AnnotationLogbackLog!";
//    }
//}
