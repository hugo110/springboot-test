package com.fanfan.example.redis.impl;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;

import com.fanfan.example.redis.IRedis;

@Configuration
@ConditionalOnProperty(name = "redisFlag", havingValue = "true")
public class RedisImp1 implements IRedis {

}
