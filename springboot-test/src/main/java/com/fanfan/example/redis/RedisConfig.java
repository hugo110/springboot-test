//package com.fanfan.example.redis;
//
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import com.fanfan.example.redis.impl.RedisImp1;
//import com.fanfan.example.redis.impl.RedisImp2;
//
//@Configuration
//public class RedisConfig {
//    @Bean
//    @Qualifier("iRedis")
//    @ConditionalOnProperty(name = "redisFlag", havingValue = "true")
//    public IRedis getRedis1() {
//        return new RedisImp1();
//    }
//
//    @Bean
//    @Qualifier("iRedis")
//    @ConditionalOnProperty(name = "redisFlag", havingValue = "false")
//    public IRedis getRedis2() {
//        return new RedisImp2();
//    }
//}
