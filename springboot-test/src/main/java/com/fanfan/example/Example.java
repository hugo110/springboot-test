//package com.fanfan.example;
//
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//  暂时全部注释，要不启动多个springapplication 麻烦
//@RestController // 注解告诉Spring以字符串的形式渲染结果，并直接返回给调用者
//@EnableAutoConfiguration // 这个注解告诉Spring Boot根据添加的jar依赖猜测你想如何配置Spring。
//public class Example {
//    @RequestMapping("/") // 注解提供路由信息。它告诉Spring任何来自"/"路径的HTTP请求都应该被映射到 home 方
//    String home() {
//        return "Hello World!";
//    }
//
//    public static void main(String[] args) throws Exception {
//        SpringApplication.run(Example.class, args); // 将业务委托给了Spring Boot的SpringApplication类。
//    }
//}