package com.fanfan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.freemarker.FreeMarkerAutoConfiguration;
import org.springframework.context.ApplicationContext;

import de.codecentric.boot.admin.config.EnableAdminServer;

/**
 * 扫描规则 如果Application类所在的包为：io.github.gefangshuai.app，则只会扫描io.github.gefangshuai.app包及其所有子包
 * 如果service或dao所在包不在io.github.gefangshuai.app及其子包下，则不会被扫描 @ComponentScan注解进行指定要扫描的包以及要扫描的类。
 */
/*@EnableSwagger2Doc
//@SpringBootApplication(exclude = FreeMarkerAutoConfiguration.class)
@SpringBootApplication()
@EnableScheduling
@EnableAsync
@ServletComponentScan*/
//@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class }) // === same as @Configuration
// @EnableAutoConfiguration
@SpringBootApplication(exclude = FreeMarkerAutoConfiguration.class) // @ComponentScan
@EnableAdminServer // spring-boot-admin 监控
public class Application {

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(Application.class, args);
        // =================================================aop测试===============
        /*        ArithmeticCalculator arithmeticCalculator = (ArithmeticCalculator) ctx.getBean("arithmeticCalculator");
        System.out.println(arithmeticCalculator.getClass());
        int result = arithmeticCalculator.add(3, 5);
        System.out.println("result: " + result);
        
        result = arithmeticCalculator.div(5, 0);
        System.out.println("result: " + result);*/

        // ============================aop end ============================
        // 启动运行
//      SpringApplication.run(Application.class, args);
//获取一个指定的bean  
        // 允许额外的参数
//        /govCreditManage/WebRoot/WEB-INF/view/index/leftBar.jsp
        System.setProperty("tomcat.util.http.parser.HttpParser.requestTargetAllow", "|{}");

//        Service service = ctx.getBean(Service.class);
        /**
         * 去掉banner
         */
//        SpringApplication app = new SpringApplication(Application.class);
////        app.setShowBanner(false); //1.3版本关闭 banner
//        app.setBannerMode(Banner.Mode.OFF);  // 1.5版本关闭 banner 
//        app.run(args);

        /**
         * 输出注入的指定的注解@Service
         */
//        ApplicationContext ctx = SpringApplication.run(Application.class, args);

//        String[] beanNames =  ctx.getBeanNamesForAnnotation(Configuration.class);
//        System.out.println("Configuration注解beanNames个数："+beanNames.length);
//        for(String bn:beanNames){
//            System.out.println(bn);
//        }

        /**
         * “ 获取所有的bean
         */
//        String[] beanNames =  ctx.getBeanDefinitionNames();
//        System.out.println("所以beanNames个数："+beanNames.length);
//        for(String bn:beanNames){
//            System.out.println(bn);
//        }
        /**
         * 热部署http://www.jb51.net/article/112473.htm
         */

    }
}