//package com.fanfan.model;
//
//import javax.persistence.Column;
//import javax.persistence.GeneratedValue;
//
//import org.hibernate.annotations.Entity;
//import org.springframework.data.annotation.Id;
//
//@Entity
//public class CityModel {
//    @Id
//    @GeneratedValue
//    private Long id;
//    @Column(nullable = false)
//    private String name;
//    @Column(nullable = false)
//    private String state;
//
//    // ... additional members, often include @OneToMany mappings
//    protected CityModel() {
//        // no-args constructor required by JPA spec
//        // this one is protected since it shouldn't be used directly
//    }
//
//    public CityModel(String name, String state) {
//        this.name = name;
//        this.state = state;
//    }
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getState() {
//        return state;
//    }
//
//    public void setState(String state) {
//        this.state = state;
//    }
//
//}
