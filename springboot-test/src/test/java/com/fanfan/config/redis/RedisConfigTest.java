package com.fanfan.config.redis;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.Data;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisConfigTest {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void test() throws Exception {
        stringRedisTemplate.opsForValue().set("aaa", "111");
        Assert.assertEquals("111", stringRedisTemplate.opsForValue().get("aaa"));
    }

    @Test
    public void testObj() throws Exception {
        User user = new User("aa@126.com", "aa", "aa123456", "aa", "123");
        ValueOperations<String, User> operations = redisTemplate.opsForValue();
        operations.set("com.neox", user);
        User user2 = operations.get("com.neox");
        System.err.println(user2);
        operations.set("com.neo.f", user, 1, TimeUnit.SECONDS);
//        Thread.sleep(1000);
//        // redisTemplate.delete("com.neo.f");
//        boolean exists = redisTemplate.hasKey("com.neo.f");
//        if (exists) {
//            System.out.println("exists is true");
//        } else {
//            System.out.println("exists is false");
//        }
        // Assert.assertEquals("aa", operations.get("com.neo.f").getUserName());
    }
}

@Data
class User {
    String string1;
    String string2;
    String string3;
    String string4;
    String string5;

    public User(String string1, String string2, String string3, String string4, String string5) {
        super();
        this.string1 = string1;
        this.string2 = string2;
        this.string3 = string3;
        this.string4 = string4;
        this.string5 = string5;
    }

}
