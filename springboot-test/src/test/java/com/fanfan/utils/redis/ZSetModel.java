package com.fanfan.utils.redis;

import java.util.Date;

import lombok.Data;

@Data
public class ZSetModel {
    private Long id;
    private String name;
    private Date createTime;
}
