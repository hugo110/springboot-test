package com.fanfan.utils.redis;

import org.springframework.data.redis.core.ZSetOperations.TypedTuple;

/**
 * 实现自定义比较器 比如根据时间排序
 * 
 * @ClassName MyDefaultTypedTuple
 * @author <a href="892042158@qq.com" target="_blank">于国帅</a>
 * @date 2019年3月8日 下午3:15:34
 *
 */
public class MyDefaultTypedTuple implements TypedTuple<ZSetModel> {

    @Override
    public int compareTo(TypedTuple<ZSetModel> arg0) {
        return 0;
    }

    @Override
    public ZSetModel getValue() {
        // Auto-generated method stub
        return null;
    }

    @Override
    public Double getScore() {
        // Auto-generated method stub
        return null;
    }

}
