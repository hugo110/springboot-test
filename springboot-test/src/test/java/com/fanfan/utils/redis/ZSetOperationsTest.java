package com.fanfan.utils.redis;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.DefaultTypedTuple;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ZSetOperationsTest {
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    private ZSetOperations<String, Object> opsForZSet;

    @Before
    public void beforeClass() {
        opsForZSet = redisTemplate.opsForZSet();
    }

    // 基础添加测试
    @Test
    public void testAdd() {
        // 将值添加到键中的排序集合，如果已存在，则更新其分数。
        opsForZSet.add("fan1", "a", 1);// true （这里的1.0可以用1代替,因为用double收参）
        opsForZSet.add("fan1", "b", 3);// true （这里的1.0可以用1代替,因为用double收参）
        opsForZSet.add("fan1", "c", 2);// true （这里的1.0可以用1代替,因为用double收参）
        System.out.println(opsForZSet.range("fan1", 0, -1));// [a, c, b]

    }

    // 复杂添加测试
    @Test
    public void testAdds() {
        ZSetOperations.TypedTuple<Object> objectTypedTuple1 = new DefaultTypedTuple<Object>("b", 2.0);// 这里必须是2.0，因为那边是用Double收参
        ZSetOperations.TypedTuple<Object> objectTypedTuple2 = new DefaultTypedTuple<Object>("c", 3.0);
        Set<ZSetOperations.TypedTuple<Object>> tuples = new HashSet<ZSetOperations.TypedTuple<Object>>();
        tuples.add(objectTypedTuple1);
        tuples.add(objectTypedTuple2);
        System.out.println(opsForZSet.add("fan1", tuples));// 2
        // 通过索引区间返回有序集合指定区间内的成员，其中有序集成员按分数值递增(从小到大)顺序排列
        System.out.println(opsForZSet.range("fan1", 0, -1));// [a, b, c]
    }

    // 复杂添加测试 自定义比较对象
    @Test
    public void testAdds2() {
        ZSetOperations.TypedTuple<Object> objectTypedTuple1 = new DefaultTypedTuple<Object>("b", 2.0);// 这里必须是2.0，因为那边是用Double收参
        ZSetOperations.TypedTuple<Object> objectTypedTuple2 = new DefaultTypedTuple<Object>("c", 3.0);
        Set<ZSetOperations.TypedTuple<Object>> tuples = new HashSet<ZSetOperations.TypedTuple<Object>>();
        tuples.add(objectTypedTuple1);
        tuples.add(objectTypedTuple2);
        System.out.println(opsForZSet.add("fan1", tuples));// 2
        // 通过索引区间返回有序集合指定区间内的成员，其中有序集成员按分数值递增(从小到大)顺序排列
        System.out.println(opsForZSet.range("fan1", 0, -1));// [a, b, c]
    }
}
