/**
 * (用一句话描述该文件做什么)
 * @Title MoreObjectActionTest.java
 * @Package com.fanfan.example.springmvc
 * @author 于国帅
 * @date 2019年3月12日 上午11:29:31
 * @version V1.0
 */
package com.fanfan.example.springmvc;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 
 * @ClassName MoreObjectActionTest
 * @author <a href="892042158@qq.com" target="_blank">于国帅</a>
 * @date 2019年3月12日 上午11:29:31
 * 
 */
public class MoreObjectActionTest {

    /**
     * 
     * @Title setUpBeforeClass
     * @author 于国帅
     * @date 2019年3月12日 上午11:29:31
     * @throws java.lang.Exception void
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * 
     * @Title tearDownAfterClass
     * @author 于国帅
     * @date 2019年3月12日 上午11:29:31
     * @throws java.lang.Exception void
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * 
     * @Title setUp
     * @author 于国帅
     * @date 2019年3月12日 上午11:29:31
     * @throws java.lang.Exception void
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * 
     * @Title tearDown
     * @author 于国帅
     * @date 2019年3月12日 上午11:29:31
     * @throws java.lang.Exception void
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for {@link com.fanfan.example.springmvc.MoreObjectAction#toTest()}.
     */
    @Test
    public void testToTest() throws Exception {
        throw new RuntimeException("not yet implemented");
    }

    /**
     * Test method for {@link com.fanfan.example.springmvc.MoreObjectAction#test()}.
     */
    @Test
    public void testTest() throws Exception {
        throw new RuntimeException("not yet implemented");
    }

}
